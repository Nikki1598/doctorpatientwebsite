$(function(){
    $(".doc-link").mouseover(function(){
        $(".doc-bg", this).css({transition: "0.5s", 
                        "background": "#00bb6d",});
        
        $(".doc-text", this).css({transition: "0.5s", 
                        "color": "white",});
        
        $(".line", this).css({transition: "0.5s", 
                        "color": "white",});
        
        $(".doc-img", this).css({transition: "0.9s", 
                        "transform": "scale(1.1)",});
        
    })
    
    $(".doc-link").mouseout(function(){
        $(".doc-bg", this).css({transition: "0.5s", 
                        "background": "#343a40",});
        
        $(".doc-text", this).css({transition: "0.5s", 
                        "color": "#00bb6d",});
        
        $(".line", this).css({transition: "0.5s", 
                        "color": "whitesmoke",});
        
        $(".doc-img", this).css({transition: "0.9s", 
                        "transform": "scale(1.0)",});
    
    })

    $(".doc-link-inner").mouseover(function(){
        $(this).css({transition: "0.5s", 
                        "color": "black",});
        
    })
    
    $(".doc-link-inner").mouseout(function(){
        $(this).css({transition: "0.5s", 
                        "color": "whitesmoke",});
        
    })

})

