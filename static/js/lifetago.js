$(function(){
    $(".dp-link").mouseover(function(){
        $(".link",this).css({transition: "0.5s", 
                        "text-decoration": "none",
                        color:"#00bb6d",});
        
        $(".link-line",this).css({transition: "0.5s", 
                            width:"60px",});
    })

    $(".dp-link").mouseout(function(){
        $(".link",this).css({transition: "0.5s", 
                        "text-decoration": "none",
                        color:"black",});
        
        $(".link-line",this).css({transition: "0.5s", 
                            width:"30px",});
    })
})


$(function(){
    $(".right-link").mouseover(function(){
        $(this).css({transition: "0.5s", 
                    "text-decoration": "none",
                    left:"10px",
                    color:"#00bb6d",});
    })

    $(".right-link").mouseout(function(){
        $(this).css({transition: "0.5s", 
                    "text-decoration": "none",
                    left:"0px",
                    color:"whitesmoke",});
    })
})


$(function(){
    $(".nav-text-dropdown").mouseover(function(){
        $(this).css({transition: "0.5s", 
                    "text-decoration": "none",
                    left:"10px",
                    color:"#00bb6d"});
    })
    $(".nav-text-dropdown").mouseout(function(){
        $(this).css({transition: "0.5s", 
                    "text-decoration": "none",
                    left:"0px",
                    color:"grey"});
    })
})

