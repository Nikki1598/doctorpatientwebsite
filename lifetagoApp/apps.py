from django.apps import AppConfig


class LifetagoappConfig(AppConfig):
    name = 'lifetagoApp'
