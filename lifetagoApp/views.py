from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect, get_object_or_404, reverse
from django.contrib.auth.models import User
from .models import SignupTable, ContactUs, Departments, Appointments, Reviews
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage
from django.http import JsonResponse
import os
from django.conf import settings

# Create your views here.
def home(request):
    # if "user_id" in request.COOKIES:
    #     uid = request.COOKIES["user_id"]
    #     usr = get_object_or_404(User,id=uid)
    #     login(request,usr)
    #     if usr.is_superuser:
    #         return HttpResponseRedirect("/admin")
    #     elif usr.is_staff:
    #         return HttpResponseRedirect("/profile-dashboard")
    #     elif usr.is_active:
    #         return HttpResponseRedirect("/patient-dashboard")
    context = {}
    doctors = SignupTable.objects.filter(user__is_staff=True,user__is_superuser=False)
    context["doctors"] = doctors
    context["active_home"] = "active"
    return render(request, 'home.html', context)

def departments(request):
    context = {}
    depart = Departments.objects.order_by("name")
    context["departments"] = depart 
    context["active_depart"] = "active"
    return render(request, 'depart.html', context)







def doctors(request):
    context = {}
    department_id = request.GET["id"]

    depart = Departments.objects.order_by("name")
    context["departments"] = depart 

    #doctors = User.objects.filter(is_staff=True,is_superuser=False).order_by("first_names")

    doctors = SignupTable.objects.filter(specialization=department_id)
    context["doctors"] = doctors
    context["len"] = len(doctors)
    context["department_name"] = Departments.objects.get(id=department_id)
    return render(request, 'doctors.html', context)

def about(request):
    context = {}
    doctors = SignupTable.objects.filter(user__is_staff=True,user__is_superuser=False)
    context["doctors"] = doctors

    total_doctors = doctors.count()
    context["total_doctors"] = total_doctors
    
    total_patients = SignupTable.objects.filter(user__is_staff=False,user__is_superuser=False).count()
    context["total_patients"] = total_patients

    total_departments = Departments.objects.all().count()
    context["total_departments"] = total_departments
    context["active_about"] = "active"
    return render(request, 'about.html', context)





    

def contact(request):
    context = {}
    if request.method == "POST":
        name = request.POST["name"]
        email = request.POST["email"]
        subject = request.POST["subject"]
        message = request.POST["message"]

        msg = ContactUs(name=name, email=email, subject=subject, message=message)
        msg.save()
        context["status"] = "Message Send Successfully!" 
    context["active_contact"] = "active"
    return render(request, 'contact.html', context)

def doctor_profile(request):
    context = {}

    doctor_id = request.GET["id"]
    doctor = get_object_or_404(SignupTable,user__id=doctor_id)
    context["doctor"] = doctor
    reviews = Reviews.objects.filter(doctor=doctor_id)
    context["reviews"] = reviews
    patient = SignupTable.objects.filter(user__is_superuser=False,user__is_staff=False)
    context["patients"] = patient
    if request.method == 'POST':
        doctor = User.objects.get(id=doctor_id)
        patient = User.objects.get(id=request.user.id)
        department = Departments.objects.get(id = SignupTable.objects.get(user__id = doctor_id).specialization.id)
        contact = request.POST["contact"]
        date = request.POST["date"]
        time = request.POST["time"]
        disease = request.POST["disease"]
        message = request.POST["message"]
        apoint = Appointments(patient=patient,doctor=doctor,department_id=department,contact=contact,date=date,time=time,disease=disease,message=message,status="pending")
        apoint.save()
        context["status"] = "Your appointment request to Dr. {} {} send successfully!".format(doctor.first_name,doctor.last_name)
    context["active_dash"] = "active"
    return render(request, 'doctorprofile.html', context)

def sign_in(request):
    context = {}
    if request.method == "POST":
        uname = request.POST["username"]
        pas = request.POST["password"]
        
        user = authenticate(username=uname, password=pas)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin/")
            elif user.is_staff:
                # res = HttpResponseRedirect("/profile-dashboard")
                # if "remember" in request.POST:
                #     res.set_cookie("user_id",user.id)
                #     res.set_cookie("date_signin",datetime.now())
                return HttpResponseRedirect("/profile-dashboard/")
            elif user.is_active:
                # res = HttpResponseRedirect("/patient-dashboard")
                # if "remember" in request.POST:
                #     res.set_cookie("user_id",user.id)
                #     res.set_cookie("date_signin",datetime.now())
                return HttpResponseRedirect("/patient-dashboard/")
        else:
           return render(request, 'registration/signin.html', {"status":"Invalid Username or Password !"} ) 
    context["active_sigin"] = "active"
    return render(request, 'registration/signin.html')

def sign_up(request):
    #depart = Departments.objects.order_by("name")
    #context["departments"] = depart
    if request.method == 'POST':
        fname = request.POST["first_name"]
        lname = request.POST["last_name"]
        uname = request.POST["username"]
        email = request.POST["email"]
        pas = request.POST["password"]
        contact = request.POST["contact_number"]
        utype = request.POST["user_type"]
        
        usr = User.objects.create_user(uname,email,pas)
        usr.first_name = fname
        usr.last_name = lname
        if utype == "doctor":
            usr.is_staff = True
        else:
            usr.is_staff = False
        usr.save()
        
        signup = SignupTable(user=usr, contact_number=contact)
        signup.save()
        user = authenticate(username=uname, password=pas)
        login(request,user)
        if user.is_staff:
            return redirect("profile_settings")
        elif user.is_active:
            return redirect("patient_settings")
        #return render(request, 'registration/signup.html', {"status":"{} Signup Successfully!".format(fname)} )
    return render(request, 'registration/signup.html')

@login_required
def profile_dashboard(request):
    context = {}
    check = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
        appointments = Appointments.objects.filter(doctor=User.objects.get(id=request.user.id))
        context["appointments"] = appointments
        patients = SignupTable.objects.filter(user__is_staff=False,user__is_superuser=False)
        context["patients"] = patients
    if request.method == "POST":
        apt_id = request.POST["apt_id"]
        status = request.POST["status"]
        appoint = Appointments.objects.get(id=apt_id)
        appoint.status = status
        appoint.save()
        if status == "Confirmed and Payment Pendding":
            context["status"] = "Your appointment with '{} {}' fixed on {}".format(appoint.patient.first_name,appoint.patient.last_name,appoint.date)
            context["alert"] = "alert-success"
        elif status == "Rejected":
            context["status"] = "Your appointment with '{} {}' canceled".format(appoint.patient.first_name,appoint.patient.last_name)
            context["alert"] = "alert-danger"
    context["active_dash"] = "active"
    context["dashboard_active"] = "active"
    return render(request, 'profiledashboard.html', context)

@login_required
def profile_mypatients(request):
    context = {}
    check = data = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
        appointments = Appointments.objects.filter(doctor=request.user.id,status="Confirmed")
        context["appointments"] = appointments
        patients = SignupTable.objects.filter(user__is_superuser=False,user__is_staff=False)
        context["patients"] = patients
    context["active_dash"] = "active"
    context["mypatient_active"] = "active"
    return render(request, 'profilemypatients.html', context)

@login_required
def profile_settings(request):
    context = {}
    check = data = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
        depart = Departments.objects.order_by("name")
        context["departments"] = depart

    if request.method == "POST":
        fname = request.POST["first_name"]
        lname = request.POST["last_name"]
        email = request.POST["email"]
        contact = request.POST["contact_number"]
        age = request.POST["age"]
        qualification = request.POST["qualification"]
        gender = request.POST["gender"]
        address_one = request.POST["address_one"]
        address_two = request.POST["address_two"]
        city = request.POST["city"]
        state = request.POST["state"]
        country = request.POST["country"]
        postal_code = request.POST["postal_code"]
        about = request.POST["about"]
        clinic_info = request.POST["clinic_info"]
        clinic_address = request.POST["clinic_address"]
        specialization = request.POST["specialization"]
        fee = request.POST["fee"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fname
        usr.last_name = lname
        usr.email = email
        usr.save()

        data.contact_number = contact
        data.age = age
        data.gender = gender
        data.qualification = qualification
        data.address_one = address_one
        data.address_two = address_two
        data.city = city
        data.state = state
        data.country = country
        data.postal_code = postal_code
        data.about = about
        data.clinic_info = clinic_info
        data.clinic_address = clinic_address
        data.fee = fee
        data.save()

        if "profile_pic" in request.FILES:
            try:
                os.remove(settings.MEDIA_ROOT+"/"+str(data.profile_pic))
            except:
                pass
            profile_pic = request.FILES["profile_pic"]
            data.profile_pic = profile_pic
            data.save()
        
        d = Departments.objects.get(id=specialization)
        data.specialization = d
        data.save()

        context["status"] = "Changes Saved Successfully!"
    
    context["active_dash"] = "active"
    context["settings_active"] = "active"
    return render(request, 'profilesettings.html', context)

def check_user(request):
    if request.method == "GET":
        uname = request.GET["uname"]
        check = User.objects.filter(username=uname)
        if(len(check) == 1):
            return HttpResponse("Exists")
    return HttpResponse("Not Exists")

def check_email(request):
    if request.method == "GET":
        email = request.GET["email"]
        check = User.objects.filter(email=email)
        if(len(check) == 1):
            return HttpResponse("Exists")
    return HttpResponse("Not Exists")

@login_required
def patient_dashboard(request):
    context = {}
    check = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
        appointments = Appointments.objects.filter(patient=User.objects.get(id=request.user.id))
        context["appointments"] = appointments
        reviews = Reviews.objects.filter(patient=User.objects.get(id=request.user.id))
        context["rev_appoint_ids"] = [i.appointment.id for i in reviews]
        doctors = SignupTable.objects.filter(user__is_superuser=False,user__is_staff=True)
        context["doctors"] = doctors
    if request.method == "POST":
        doctor_id = request.POST["doctor_id"]
        doctor_id = User.objects.get(id=doctor_id)
        patient_id = User.objects.get(id=request.user.id)
        appoint_id = request.POST["appoint_id"]
        appoint_id = Appointments.objects.get(id=appoint_id)
        subject = request.POST["subject"]
        comment = request.POST["comment"]
        review = Reviews(patient=patient_id, doctor=doctor_id, appointment=appoint_id, subject=subject, comment=comment)
        review.save()
        context["status"] = "Review submit successfully!"
    context["active_dash"] = "active"
    context["dashboard_active"] = "active"
    return render(request, 'patientdashboard.html', context)

@login_required
def patient_settings(request):
    context = {}
    check = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method == "POST":
        fname = request.POST["first_name"]
        lname = request.POST["last_name"]
        email = request.POST["email"]
        contact = request.POST["contact_number"]
        age = request.POST["age"]
        gender = request.POST["gender"]
        address_one = request.POST["address_one"]
        address_two = request.POST["address_two"]
        city = request.POST["city"]
        state = request.POST["state"]
        country = request.POST["country"]
        postal_code = request.POST["postal_code"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fname
        usr.last_name = lname
        usr.email = email
        usr.save()

        data.contact_number = contact
        data.age = age
        data.gender = gender
        data.address_one = address_one
        data.address_two = address_two
        data.city = city
        data.state = state
        data.country = country
        data.postal_code = postal_code
        data.save()

        if "profile_pic" in request.FILES:
            try:
                os.remove(settings.MEDIA_ROOT+"/"+str(data.profile_pic))
            except:
                pass
            profile_pic = request.FILES["profile_pic"]
            data.profile_pic = profile_pic
            data.save()
        
        context["status"] = "Changes Saved Successfully!"
    context["active_dash"] = "active"
    context["settings_active"] = "active"
    return render(request, 'patientsettings.html', context)

@login_required
def user_sign_out(request):
    logout(request)
    return HttpResponseRedirect("/")

@login_required
def change_password(request):
    context = {}
    data = SignupTable.objects.get(user__id=request.user.id)
    context["data"] = data
    if request.method == "POST":
        current = request.POST["cpwd"]
        new_pas = request.POST["npwd"]

        user = User.objects.get(id=request.user.id)
        un = user.username
        check = user.check_password(current)
        
        if check == True:
            user.set_password(new_pas)
            user.save()
            
            context["msg"] = "Password Changed Successfully!"
            context["alert"] = "alert-success"
            
            user = User.objects.get(username=un)
            login(request,user)
        else:
            context["msg"] = "Incorrect Current Password!"
            context["alert"] = "alert-danger"
    context["active_dash"] = "active"
    context["change_pass_active"] = "active"
    return render(request, "registration/change_password.html", context)

@login_required
def profile_reviews(request):
    context = {}
    check = data = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
        reviews = Reviews.objects.filter(doctor=request.user.id)
        context["reviews"] = reviews
        patients = SignupTable.objects.filter(user__is_superuser=False,user__is_staff=False)
        context["patients"] = patients
    context["active_dash"] = "active"
    context["reviews_active"] = "active"
    return render(request, "profile_reviews.html", context)

def all_doctors(request):
    context = {}
    depart = Departments.objects.order_by("name")
    context["departments"] = depart 

    doctors = SignupTable.objects.order_by("user__first_name")
    context["doctors"] = doctors
    context["len"] = len(doctors)
    context["department_name"] = "Doctors"

    if "q_name" in request.GET or "q_city" in request.GET:
        q_name = request.GET["q_name"]
        q_city = request.GET["q_city"]
        #doctors = SignupTable.objects.filter(user__first_name__contains=q_name)
        doctors = SignupTable.objects.filter(Q(user__first_name__contains=q_name)&Q(clinic_address__contains=q_city))
        context["doctors"] = doctors
        context["len"] = len(doctors)
    return render(request, 'all_doctors.html', context)

@login_required
def send_emails(request):
    context = {}
    check = SignupTable.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = SignupTable.objects.get(user__id=request.user.id)
        context["data"] = data
    if request.method == "POST":
        to = request.POST["to"].split(",")
        subject = request.POST["subject"]
        message = request.POST["message"]
        name = request.user.first_name+" "+request.user.last_name
        try:
            em = EmailMessage(subject,message,name,to=to)
            em.send()
            context["status"] = "Email Sent Successfully!"
            context["alert"] = 'alert-success'
        except:
            context["status"] = "Email Could Not Send! Please check your Internet connection / Email Address!"
            context["alert"] = 'alert-danger'
    context["active_dash"] = "active"
    context["emails_active"] = "active"
    return render(request, "sendemails.html", context)

def password_forget(request):
    context = {}
    if request.method == "POST":
        un = request.POST["username"]
        new_pass = request.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(new_pass)
        user.save()
        
        context["status"] = "Password Changed Successfully!"
    context["active_sigin"] = "active"
    return render(request, 'registration/password_reset.html',context)

import random

def password_reset(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(User,username=un)
        otp = random.randint(1000,9999)
        msz = "Dear {}, \n {} is your One Time Password (OTP). Do not share with others. \n\nThanks & Regards, \nLIFETAGO Team".format(user.username,otp)
        try:
            email = EmailMessage("Reset Password",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"otp":otp})
        except:
            return JsonResponse({"status":"error","email":user.email})
    except:
        return JsonResponse({"status":"failed"})

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
from currency_converter import CurrencyConverter
@login_required
def process_payment(request):
    c = CurrencyConverter()
    host = request.get_host()
    apid = request.GET["apid"]
    appointment = get_object_or_404(Appointments,id=apid)
    doctor_name = "Dr. "+appointment.doctor.first_name+" "+appointment.doctor.last_name
    fee = get_object_or_404(SignupTable,user__id=appointment.doctor.id).fee
    fee = round(c.convert(fee, 'INR', 'USD'), 2)
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(fee),
        'item_name': doctor_name,
        'invoice': "INV"+str(apid),
        'currency_code': 'USD',
        'notify_url': 'http://{}{}'.format(host,
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}?apid={}'.format(host,
                                           reverse('payment_done'),apid),
        'cancel_return': 'http://{}{}'.format(host,
                                              reverse('payment_cancelled')),
    }
    
    request.session["appointment_id"] = apid

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'payment/process_payment.html', {'form': form})

@login_required
def payment_done(request):
    if "appointment_id" in request.session:
        apid = request.session["appointment_id"]
        appointment = get_object_or_404(Appointments,id=apid)
        appointment.status = "Confirmed"
        appointment.save()
    return render(request, 'payment/payment_done.html')

@login_required
def payment_cancelled(request):
    return render(request, 'payment/payment_cancelled.html')

# def messages(request):
#     context = {}
#     check = data = SignupTable.objects.filter(user__id=request.user.id)
#     if len(check)>0:
#         data = SignupTable.objects.get(user__id=request.user.id)
#         context["data"] = data
#     context["messages_active"] = "active"
#     return render(request, 'messages.html', context)

