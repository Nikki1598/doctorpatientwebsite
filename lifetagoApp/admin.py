from django.contrib import admin
from .models import SignupTable, ContactUs, Departments, Appointments, Reviews
# Register your models here.

admin.site.site_header = "LIFETAGO"

admin.site.register(SignupTable)



class ContactUsAdmin(admin.ModelAdmin):
    # fields = ["name","email"]
    list_display = ["name", "email", "subject", "add_on"]
    search_fields = ["name"]
    list_filter = ["add_on"]

class DepartmentsAdmin(admin.ModelAdmin):
    list_display = ["name", "added_on"]

class AppointmentsAdmin(admin.ModelAdmin):
    list_display = ["doctor","patient","added_on"]
    list_filter = ["added_on","doctor"]

class ReviewsAdmin(admin.ModelAdmin):
    list_display = ["doctor","patient","subject","added_on"]
    list_filter = ["added_on","doctor"]

admin.site.register(ContactUs, ContactUsAdmin)
admin.site.register(Departments, DepartmentsAdmin)
admin.site.register(Appointments, AppointmentsAdmin)
admin.site.register(Reviews, ReviewsAdmin)
