from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Departments(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to = "Departments/%Y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Departments"

class SignupTable(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number = models.CharField(max_length=250)
    profile_pic =models.ImageField(upload_to = "profiles/%Y/%m/%d",null=True,blank=True)
    specialization = models.ForeignKey(Departments,on_delete=models.CASCADE,null=True,blank=True,default="")
    #specialization = models.CharField(max_length=100,null=True,blank=True)
    address_one = models.CharField(max_length=100,null=True,blank=True)
    address_two = models.CharField(max_length=100,null=True,blank=True)
    city = models.CharField(max_length=250, null=True, blank=True)
    state = models.CharField(max_length=250, null=True, blank=True)
    country = models.CharField(max_length=250, null=True, blank=True)
    postal_code = models.CharField(max_length=250, null=True, blank=True)
    about = models.TextField(blank=True,null=True)
    gender = models.CharField(max_length=100,null=True,default="Male")
    age = models.CharField(max_length=100,null=True,blank=True)
    clinic_info = models.CharField(max_length=250,null=True,blank=True)
    clinic_address = models.CharField(max_length=250,null=True,blank=True)
    add_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)
    fee = models.FloatField(null=True,blank=True)
    def __str__(self):
        return self.user.username
    class Meta:
        verbose_name_plural = "Sign-up"

class ContactUs(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    subject = models.CharField(max_length=100)
    message = models.TextField()
    add_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.email
    class Meta:
        verbose_name_plural = "Feedbacks"

class Appointments(models.Model):
    patient = models.ForeignKey(User, limit_choices_to={'is_staff': False},on_delete=models.CASCADE,related_name='appointment_patient',null=True) 
    doctor = models.ForeignKey(User, limit_choices_to={'is_superuser': False,'is_staff':True},on_delete=models.CASCADE,related_name='appointment_doctor',null=True)
    department_id = models.ForeignKey(Departments,on_delete=models.CASCADE)
    contact = models.IntegerField()
    date = models.DateField()
    time = models.TimeField()
    disease = models.CharField(max_length=250,null=True,blank=True)
    message = models.TextField(blank=True)
    status = models.CharField(max_length=100)
    added_on = models.DateTimeField(auto_now=True) 
    class Meta:
        verbose_name_plural = "Appointments"

class Reviews(models.Model):
    patient = models.ForeignKey(User, limit_choices_to={'is_staff': False},on_delete=models.CASCADE,related_name='review_patient') 
    doctor = models.ForeignKey(User, limit_choices_to={'is_superuser': False,'is_staff':True},on_delete=models.CASCADE,related_name='review_doctor')
    appointment = models.ForeignKey(Appointments,on_delete=models.CASCADE, null=True, blank=True)
    subject = models.CharField(max_length=100)
    comment = models.TextField()
    added_on = models.DateTimeField(auto_now=True)
    class Meta:
        verbose_name_plural = "Reviews"

# class Messages(models.Model):
#     patient_id = models.ForeignKey(User, limit_choices_to={'is_staff': False},on_delete=models.CASCADE,related_name='msg_patient') 
#     doctor_id = models.ForeignKey(User, limit_choices_to={'is_superuser': False,'is_staff':True},on_delete=models.CASCADE,related_name='msg_doctor') 
#     message = models.CharField(max_length=1000)
#     status = models.BooleanField(default=False)
#     processed_on = models.DateTimeField(auto_now=True)
#     def __str__(self):
#         return self.patient_id
#     class Meta:
#         verbose_name_plural = "Messages"