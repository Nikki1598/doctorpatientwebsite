# Generated by Django 3.0.5 on 2020-04-30 09:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lifetagoApp', '0019_auto_20200427_0116'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviews',
            name='appointment',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='lifetagoApp.Appointments'),
        ),
    ]
