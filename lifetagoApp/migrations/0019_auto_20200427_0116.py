# Generated by Django 3.0.5 on 2020-04-26 19:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lifetagoApp', '0018_merge_20200427_0113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointments',
            name='doctor_id',
        ),
        migrations.RemoveField(
            model_name='appointments',
            name='patient_id',
        ),
        migrations.AddField(
            model_name='appointments',
            name='doctor',
            field=models.ForeignKey(limit_choices_to={'is_staff': True, 'is_superuser': False}, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='appointment_doctor', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='appointments',
            name='patient',
            field=models.ForeignKey(limit_choices_to={'is_staff': False}, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='appointment_patient', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='signuptable',
            name='specialization',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='lifetagoApp.Departments'),
        ),
        migrations.CreateModel(
            name='Reviews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=100)),
                ('comment', models.TextField()),
                ('added_on', models.DateTimeField(auto_now=True)),
                ('doctor', models.ForeignKey(limit_choices_to={'is_staff': True, 'is_superuser': False}, on_delete=django.db.models.deletion.CASCADE, related_name='review_doctor', to=settings.AUTH_USER_MODEL)),
                ('patient', models.ForeignKey(limit_choices_to={'is_staff': False}, on_delete=django.db.models.deletion.CASCADE, related_name='review_patient', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Reviews',
            },
        ),
    ]
