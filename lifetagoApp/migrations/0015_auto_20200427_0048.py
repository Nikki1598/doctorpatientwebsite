# Generated by Django 3.0.5 on 2020-04-26 19:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lifetagoApp', '0014_auto_20200426_2343'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contactus',
            options={'verbose_name_plural': 'Feedbacks'},
        ),
        migrations.AlterModelOptions(
            name='signuptable',
            options={'verbose_name_plural': 'Sign-up'},
        ),
        migrations.CreateModel(
            name='Appointments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact', models.IntegerField()),
                ('date', models.DateField()),
                ('time', models.TimeField()),
                ('message', models.TextField(blank=True)),
                ('status', models.CharField(max_length=100)),
                ('added_on', models.DateTimeField(auto_now=True)),
                ('department_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lifetagoApp.Departments')),
                ('doctor_id', models.ForeignKey(limit_choices_to={'is_staff': True}, on_delete=django.db.models.deletion.CASCADE, related_name='doctor', to=settings.AUTH_USER_MODEL)),
                ('patient_id', models.ForeignKey(limit_choices_to={'is_staff': False}, on_delete=django.db.models.deletion.CASCADE, related_name='patient', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Appointments',
            },
        ),
    ]
